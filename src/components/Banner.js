import React from "react";
import { Jumbotron, Row, Col, Button } from "react-bootstrap";
import { Link } from "react-router-dom";

export default function Banner({ bannerProp }) {
  return (
    <Row>
      <Col>
        <Jumbotron className="text-center my-5 jumbo">
          <h1 className="f-txt">
            <strong>{bannerProp.title}</strong>
          </h1>
          <h4 className="mt-4">{bannerProp.description}</h4>
          <Link className="btn btn-info mt-5" to={bannerProp.destination}>
            {bannerProp.buttonToCall}
          </Link>
        </Jumbotron>
      </Col>
    </Row>
  );
}
