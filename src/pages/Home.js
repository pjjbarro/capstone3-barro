import React, { useEffect, useState } from "react";

import Banner from "../components/Banner";

import { Link } from "react-router-dom";

import { Card, Button, Row, Col } from "react-bootstrap";

export default function Home() {
  const [products, setProducts] = useState([]);

  let bannerComponent = {
    title: "Motorcycle Parts",
    description: "Your Trusted Partner to Buy for Your Motorcycle ",
    buttonToCall: "Buy Now!",
    destination: "/login",
  };

  useEffect(() => {
    fetch("http://localhost:8000/products/getP")
      .then((res) => res.json())
      .then((data) => {
        setProducts(
          data.map((product) => {
            if (product.price > 2000) {
              return (
                <div key={product._id} className="text-center mb-5 crdHome">
                  <Row>
                    <Col>
                      <Card crdHome>
                        <Card.Header>Featured</Card.Header>
                        <Card.Body>
                          <Card.Title>{product.name}</Card.Title>
                        </Card.Body>
                        <Card.Footer className="text-muted">
                          <Link
                            className="btn btn-info"
                            to={`/products/${product._id}`}
                          >
                            View Parts
                          </Link>
                        </Card.Footer>
                      </Card>
                    </Col>
                  </Row>
                </div>
              );
            }
          })
        );
      });
  }, []);

  return (
    <>
      <Banner bannerProp={bannerComponent} />

      <h1 className="text-center my-5 text-info">Featured Products</h1>

      {products}
    </>
  );
}
