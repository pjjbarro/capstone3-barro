import React, { useState, useContext, useEffect } from "react";

import { Card, Button, Row, Col, Jumbotron } from "react-bootstrap";

import { useParams, useHistory, Link } from "react-router-dom";

import Swal from "sweetalert2";

import UserContext from "../userContext";

import Product from "../components/Products";

export default function ViewParts() {
  const { productId } = useParams();

  const { user, addToCart } = useContext(UserContext);

  const history = useHistory();

  const [partDetails, setPartDetails] = useState({
    name: null,
    description: null,
    price: null,
  });

  useEffect(() => {
    fetch(`http://localhost:8000/products/getSingleP/${productId}`)
      .then((res) => res.json())
      .then((data) => {
        if (data.message) {
          Swal.fire({
            icon: "error",
            title: "Part is Unavailable",
            text: data.message,
          });
        } else {
          setPartDetails({
            id: data._id,
            name: data.name,
            description: data.description,
            price: data.price,
          });
        }
      });
  }, [productId]);

  return (
    <Row>
      <Col>
        <Jumbotron className="text-center mt-5 viewJumbo">
          <h4 className="text-left mb-5 txtColorGame">
            PHP {partDetails.price}
          </h4>
          <h1 className="txtColorGame">{partDetails.name}</h1>
          <p className="my-5 txtColorGame">{partDetails.description}</p>

          {user.isAdmin === false ? (
            <>
              <Button
                className="btn btn-info mx-3"
                onClick={() => addToCart(partDetails)}
              >
                {" "}
                Buy Parts
              </Button>
              <Link className="btn btn-light" to="/products">
                Go Back
              </Link>
            </>
          ) : (
            <Link className="btn btn-info btn-lg" to="/login">
              Login
            </Link>
          )}
        </Jumbotron>
      </Col>
    </Row>
  );
}
